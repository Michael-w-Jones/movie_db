var gulp = require('gulp');
var sass = require('gulp-sass');
var wait = require('gulp-wait');

gulp.task('styles', function() {
    gulp.src('src/sass/**/*.scss')
        .pipe(wait(1500))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css/'));
});

gulp.task('images', function() {
  return gulp.src('./images/**/*')
    .pipe(gulp.dest('dist/images'))
});


gulp.task('watch-sass',function() {
  gulp.watch('src/sass/**/*.scss',['styles']);
  gulp.start('images');
});

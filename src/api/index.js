export const getMovieData = (data, page) => {

    const url = 'https://api.themoviedb.org/3/';
    const key = 'api_key=fde10b96a7cea514046dbbeee9cbe3f6';
    return fetch(url + data + key + page)
        .then((response) => response.json())
        .catch(error => console.warn(error))
}
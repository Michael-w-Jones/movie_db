import React from 'react';
import {getMovieData} from '../api';
import Movie from '../Components/Movie';
import Buttons from '../Components/Buttons';

class Main extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: this.props.search,
            page: 1,
            defaultGet: 'discover/movie?primary_release_date.gte=2018-05-15&primary_release_date.lte=2019-05-22&',
            currentGet: null,
            searchData: false
        }

        this.next = this.next.bind(this);
        this.prev = this.prev.bind(this);
    }

    componentDidMount() {  
        getMovieData(this.state.defaultGet, '&page='+this.state.page)  
        .then((responseData) => {
          this.setState({data: responseData.results})
        })        
    }

    prev() { 

        if(this.state.page !== 1) {
            this.setState({page: this.state.page - 1}, function() {
                getMovieData(this.state.defaultGet, '&page='+this.state.page)  
                .then((responseData) => {
                    this.setState({data: responseData.results})
                })  
            }); 
        } 
    }

    next() {

        let newPage = '';
       
        if(!this.props.get) {
            newPage = this.state.defaultGet;
        }else{
            newPage = this.props.get;
        };

        this.setState({page: this.state.page + 1}, function () {
            getMovieData(newPage, '&page='+this.state.page)
            .then((responseData) => {
                this.setState({data: responseData.results, searchData: true}, console.log(this.state.data) )
            })   
        });
    }
  
    render() {
        
        if(this.props.search && !this.state.searchData) {

            if(this.props.search.length > 0) {
            
                let list = this.props.search;
                const movies = list.map((movie, i) => {
                    if (movie.poster_path === null){
                        movie.poster_path = '/iUcufgPM8X1dmEtuQBXKsV2asnB.jpg';
                    }
                    return (
                        <Movie id={movie.id} key={movie.title+i} title={movie.title} date={movie.release_date} poster={movie.poster_path} />
                    )
                }) 
                return (
                    <div className="wrapper">
                         <div className="top">
                            <h1>Search Results</h1>
                            <Buttons page={this.state.page} prev={this.prev} next={this.next} />
                        </div>
                        <div className="movie-container">
                            {movies}
                        </div>
                        <Buttons page={this.state.page} prev={this.prev} next={this.next} />
                    </div>
                )
            } else {
                return(
                    <div className="wrapper">
                        <h1>Sorry your criteria has returned no results</h1>
                    </div>
                )
            }
        } else

        if(this.state.data) {

            let list = this.state.data;

            const movies = list.map((movie, i) => {
                if (movie.poster_path === null){
                    movie.poster_path = '/iUcufgPM8X1dmEtuQBXKsV2asnB.jpg';
                }
                return (
                    <Movie id={movie.id} key={movie.title+i} title={movie.title} date={movie.release_date} poster={movie.poster_path} />
                )
            }) 

            return (
                <div className="wrapper">
                    <div className="top">
                        <h1>Upcoming Movies</h1>
                            <Buttons page={this.state.page} prev={this.prev} next={this.next} />
                        </div>
                    <div className="movie-container">
                        {movies}
                    </div>
                    <Buttons prev={this.prev} next={this.next} />
                </div>
            )
        }

        return (
            <div className="wrapper">
                <div className="movie-conatiner">
                    
                    <h1>Loading...</h1>
                </div>
            </div>
        )
    }
}

export default Main
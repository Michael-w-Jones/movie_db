import React from 'react'
import {getMovieData} from '../api';

class Info extends React.Component {

    constructor() {
        super();

        this.state = {

            title: '',
            releaseDate: '',
            tagline: '',
            overview: '',
            img: '',
            runtime: '',
            voteAverage: 0,
            still: '',
            imagePath: 'https://image.tmdb.org/t/p/w500'
        }
    }

    componentDidMount() {

        const movie = this.getUrlParams();
        getMovieData('movie/'+movie.id+"?", "&page=1")
        .then((responseData) => {
            if(!responseData.backdrop_path){
                this.setState({still: '/no-image.jpg', imagePath: '../images'})
            }else{
            this.setState({still: responseData.backdrop_path})
            }
            this.setState({
                title: responseData.title,
                release_date: responseData.release_date, 
                tagline: responseData.tagline, 
                overview: responseData.overview, 
                img: responseData.poster_path,
                runtime: responseData.runtime,
                voteAverage: responseData.vote_average
            })
        }) 
    }

    getUrlParams(prop) {
        const params = {};
        const search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
        const definitions = search.split( '&' );
    
        definitions.forEach( function( val, key ) {
            var parts = val.split( '=', 2 );
            params[ parts[0] ] = parts[ 1 ];
        } );
    
        return (prop && prop in params) ? params[prop] : params;
    }

    render() {

        let ratings = ["score"]
        const score = this.state.voteAverage;
        if(score < 5) {
            ratings.push("bad");
        }else if(score > 7.5) {
            ratings.push("good");
        }else{
           ratings.push("ok"); 
        }
   
        return (
            <div className="details-container">
                <div className="headers">
                    <h3 className="movie-title">{this.state.title}</h3>
                    <h4 className="tagline">{this.state.tagline}</h4>
                </div>
                <div className={ratings.join(" ")}>
                    Rating
                    <span>{this.state.voteAverage}</span>
                </div>
                <div className="poster">
                   <img className="movie-still" src={this.state.imagePath + this.state.still} alt={this.state.title + " poster"} />
                   <p className="description">{this.state.overview}</p>
                </div>
                <div>
                   
                    <p className="general"><span>Runtime: </span>{this.state.runtime} minutes</p>
                    <p className="general"><span>Release date: </span>{this.state.release_date}</p>
                </div>

            </div>
        )
    }
}

export default Info
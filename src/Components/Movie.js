import React from 'react';
import {Link} from 'react-router-dom';

class Movie extends React.Component {

    render() {

        return (
            <div className="movie-box">
                <div className="box-inner">
                    <Link to={"/movie-details?id=" + this.props.id} className="movie-details">
                        <div className="poster-box">
                            <img className="poster" src={'https://image.tmdb.org/t/p/w500' + this.props.poster} alt={this.props.title}/>
                        </div>
                        <div className="movie-info">
                            <h3 className="title"><span className="bold">Title: </span> {this.props.title}</h3>
                            <p className="info release"><span className="bold">Release: </span> {this.props.date}</p>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

export default Movie;
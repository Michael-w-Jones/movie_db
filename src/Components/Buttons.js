import React from 'react';

class Buttons extends React.Component {
     
    render() {
         return (
             <div className="button-container">
                 <button onClick={this.props.prev} className="button prev">
                    Prev
                </button>
                <button onClick={this.props.next} className="button next">
                    Next
                </button>
            </div>
         )
    }
}

export default Buttons;
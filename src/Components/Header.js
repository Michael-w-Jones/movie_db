import React from 'react';
import logo from '../images/movieDB-logo.svg';
import { Link } from 'react-router-dom';

class Header extends React.Component {

    constructor() {
        super();

        this.state = {
            title: '',
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
       
        this.setState({
           title: e.target.value
       })
    }

    render() {
        let currentPath = window.location.pathname;
        return(
            <div className="header">
                <Link to="/" onClick={this.props.home}><img src={logo} alt="The Movie Logo" width="60" /></Link>
                <div className="buttons">
                    {  currentPath !== '/'
                        ? ( <Link className="back" to="/">Back to seach results</Link>)
                        :  
                    (<form className="search" onSubmit={this.props.submit}>
                        <input className="title" id="title" name="title" placeholder="Search" type="text" ref={this.props.inputRef} onChange={this.handleChange} value={this.state.title} pattern="[^()/><\][\\\x22,;|]+" maxLength="20" required />
                        <input className="submit" id="submit" type="submit" value=" " />
                    </form> )
                    }  
                </div>
            </div>
        )
    }
}

export default Header
import React from 'react';
import Header from './Components/Header';
import { Switch, Route } from 'react-router-dom';
import Main from './Pages/Main';
import Info from './Pages/Info';
import {getMovieData} from './api';

class App extends React.Component {

  constructor(){
    super();

    this.state = {
        data: null,
        defaultGet: 'discover/movie?primary_release_date.gte=2018-05-15&primary_release_date.lte=2019-05-22&',
        currentGet: null,
    }

    this.homebutton = this.homebutton.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   
  }

  homebutton() {
    this.setState({currentGet: null}, function() {
      getMovieData(this.state.defaultGet, "&page=1")  
      .then((responseData) => {
        this.setState({data: responseData.results}, function(){
        })
      })    
    })
  }

  handleSubmit(e) {

    e.preventDefault();
    let searchQuery = this.textInput.value
    this.setState({currentGet: 'search/movie?query='+searchQuery+'&'}, function() {
      getMovieData(this.state.currentGet, '&page=1')
        .then((responseData) => {
            this.setState({data: responseData.results})
          })  
    }) 
  }

  render() {

    const movies = this.state.data;

    return (
      <div className="App">
        <Header submit={this.handleSubmit} home={this.homebutton} inputRef={(input) => this.textInput = input} />
        <Switch>
          <Route exact path='/' render={() => <Main search={movies} get={this.state.currentGet} /> } />
          <Route path="/movie-details" render={() => <Info />} />
        </Switch>
      </div>
    );
  }
}

export default App;
